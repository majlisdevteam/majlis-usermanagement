/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.dmssw.model.MajlisMobileUsers;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import util.LdapUser;
import util.ResponseData;
import util.ResponseWrapper;

/**
 *
 * @author Dilhan Ganegama
 */
public class UserControllerTest {
    
    public UserControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of validateCmsUser method, of class UserController.
     */
    @Test
    public void testValidateCmsUser() {
        System.out.println("validateCmsUser");
        LdapUser user = null;
        String system = "";
        UserController instance = new UserController();
        ResponseWrapper expResult = null;
        ResponseWrapper result = instance.validateCmsUser(user, system);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getGroups method, of class UserController.
     */
    @Test
    public void testGetGroups() {
        System.out.println("getGroups");
        String start = "";
        String limit = "";
        String user_id = "";
        String room = "";
        String department = "";
        String branch = "";
        String countryCode = "";
        String division = "";
        String organaization = "";
        String system = "";
        ResponseWrapper expResult = null;
        ResponseWrapper result = UserController.getGroups(start, limit, user_id, room, department, branch, countryCode, division, organaization, system);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of createMobileUserDetails method, of class UserController.
     */
    @Test
    public void testCreateMobileUserDetails() {
        System.out.println("createMobileUserDetails");
        MajlisMobileUsers m_user = new MajlisMobileUsers(Integer.BYTES, "1234567884721", "mobileUserName", "mobileUserEmail", null, "country", "mobileUserProvince", "mobileUserCity", null, null, null, null);
        UserController instance = new UserController();
        ResponseData expResult = new ResponseData(null, 1, "26015", null);;
        ResponseData result = instance.createMobileUserDetails(m_user);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of modifyMobileUserDetails method, of class UserController.
     */
    @Test
    public void testModifyMobileUserDetails() {
        System.out.println("modifyMobileUserDetails");
        MajlisMobileUsers m_user = new MajlisMobileUsers(Integer.BYTES, "1234567884701", "mobileUserName", "mobileUserEmail", null, "country", "mobileUserProvince", "mobileUserCity", null, null, null, null);
           
        
        UserController instance = new UserController();
       // ResponseData rd = new ResponseData("");
        ResponseData expResult =new ResponseData(null, 0, "26015", null);
        
        ResponseData result = instance.modifyMobileUserDetails(m_user);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
