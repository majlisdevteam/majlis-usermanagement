/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import java.util.Set;
import service.Users;

/**
 *
 * Application API configuration class for User Management microservice
 * 
 * @version 1.0 / 20 March 2017
 * @author Nadishan Amarasekara
 */
@javax.ws.rs.ApplicationPath("service")
public class AppConfig extends javax.ws.rs.core.Application{
    
       @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.dmssw.service.SessionTest.class);
        resources.add(service.Users.class);
    }
    
}
