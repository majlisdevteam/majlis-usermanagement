/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;
import java.util.List;

/**
 *
 * @version 1.0 / 15 March 2017
 * @author Sandali Kaushalya
 */
public class ResponseData {

    private Object responseCode;
    private int responseFlag;
    private String passCode;
    
    private Object responseData;

    public ResponseData(Object responseCode, int responseFlag, String passCode, Object responseData) {
        this.responseCode = responseCode;
        this.responseFlag = responseFlag;
        this.passCode = passCode;
        this.responseData = responseData;
    }

    public Object getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Object responseCode) {
        this.responseCode = responseCode;
    }

    public Object getResponseData() {
        return responseData;
    }

    public void setResponseData(Object responseData) {
        this.responseData = responseData;
    }

    /**
     * @return the responseFlag
     */
    public int getResponseFlag() {
        return responseFlag;
    }

    /**
     * @param responseFlag the responseFlag to set
     */
    public void setResponseFlag(int responseFlag) {
        this.responseFlag = responseFlag;
    }

    /**
     * @return the passCode
     */
    public String getPassCode() {
        return passCode;
    }

    /**
     * @param passCode the passCode to set
     */
    public void setPassCode(String passCode) {
        this.passCode = passCode;
    }

   
    
    
    
    
}
