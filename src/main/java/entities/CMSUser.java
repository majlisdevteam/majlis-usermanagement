package entities;

import java.util.Date;

public class CMSUser {

    private Date expiryDate;
    private int status;
    private int allowedNotificationCnt;
    private String name;
    private String password;
    private String email;
    private Group userType;
    private int[] groupId;
    private Date dateInserted;
    private Date dateModified;

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setAllowedNotificationCnt(int allowedNotificationCnt) {
        this.allowedNotificationCnt = allowedNotificationCnt;
    }

    public int getAllowedNotificationCnt() {
        return allowedNotificationCnt;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setUserType(Group userType) {
        this.userType = userType;
    }

    public Group getUserType() {
        return userType;
    }

    public void setGroupId(int[] groupId) {
        this.groupId = groupId;
    }

    public int[] getGroupId() {
        return groupId;
    }

    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    public Date getDateInserted() {
        return dateInserted;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public Date getDateModified() {
        return dateModified;
    }
}
