/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.dmssw.model.MajlisMobileUsers;
import controller.LDAPWSController;
import controller.UserController;
import entities.MobileUser;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import util.LdapUser;
import util.ResponseWrapper;

/**
 *
 * This service describes about the user based services
 *
 * @version 1.0 / 20 March 2017
 * @author Nadishan Amarasekara
 */
@Path("/users")
public class Users {

    @POST
    @Path("/validateCmsUser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response validateCmsUser(LdapUser user,
            @HeaderParam("system") String system ) {

        UserController userController = new UserController();
     
        return Response.status(Response.Status.OK).entity(userController.validateCmsUser(user, system)).build();
    }

    @GET
    @Path("/getAllGroups")
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseWrapper getGroupsDetials(@QueryParam("start") @DefaultValue("all") String start,
            @QueryParam("limit") @DefaultValue("all") String limit,
            @HeaderParam("userId") String userId, 
            @HeaderParam("room")  String room, 
            @HeaderParam("department")  String department, 
            @HeaderParam("branch")  String branch, 
            @HeaderParam("countryCode") String countryCode, 
            @HeaderParam("division")  String division, 
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system){
        return UserController.getGroups(start,limit,userId,room,department,branch,countryCode,division,organization,system);
    }
    
        
    @GET
    @Path("/getAllUsers")
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseWrapper getAllUsers(@HeaderParam("system") String system) {
        LDAPWSController ldapcontroller = new LDAPWSController();
 
        return ldapcontroller.getAllUsers(system);
    }
    
    
    
    @POST
    @Path("/createMobileUser")
    @Produces({MediaType.APPLICATION_JSON})
    public Response createMobileUserDetails(MajlisMobileUsers m_user) {
 
        UserController usercontroller = new UserController();
        return Response.status(Response.Status.OK).entity(usercontroller.createMobileUserDetails(m_user)).build();
    }
    
    @POST
    @Path("/modifyMobileUserDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public Response modifyMobileUser(MajlisMobileUsers m_user) {
 
        UserController usercontroller = new UserController();
        return Response.status(Response.Status.OK).entity(usercontroller.modifyMobileUserDetails(m_user)).build();
    }
    
    
}
