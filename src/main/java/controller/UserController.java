/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.dmssw.controller.ORMConnection;
import com.dmssw.model.MajlisMobileUsers;
import entities.Group;
import entities.MobileUser;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.query.Query;
import util.LdapUser;
import util.Passcode;
import util.ResponseData;
import util.ResponseWrapper;

/**
 *
 * @version 1.0 / 20 March 2017
 * @author Nadishan Amarasekara
 */
public class UserController {

    public ResponseWrapper validateCmsUser(LdapUser user, String system) {

        System.out.println("validateCmsUser method call.................");

        return LDAPWSController.login(user.getUserId(), user.getPassword(), "MajlisCMS");

    }

    ////////////////////////////sandali//////////////////////////////////////////////////////////
    public static ResponseWrapper getGroups(String start,
            String limit,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization, String system) {

        System.out.println("get Groups Method Call...........");
        ResponseWrapper rw = null;
        String g = null;
        ArrayList<Group> gp = new ArrayList<>();
        try {
            System.out.println("Call LDAP Get Groups.............");
            rw = LDAPWSController.getAllGroupsInSystem(start, limit, user_id, room, department, branch, countryCode, division, organaization, system);

        } catch (Exception ex) {
            System.out.println("Error in get Groups Method :  " + ex.getMessage());
        }

        System.out.println("Successfully Return Value.............");

        System.out.println("-----------------------------------------------------");
        return rw;
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////

    public ResponseData createMobileUserDetails(MajlisMobileUsers m_user) {

        System.out.println("MobileUserDetails method call.................");

        ResponseData rd = new ResponseData(null,0,"",null);
        String userName = m_user.getMobileUserName();

        int i = 0;
        String strPassCodeNO = "";
        try {
            if (userName != null) {
                String mobileNo = m_user.getMobileUserMobileNo();
                System.out.println("mobile no" + mobileNo);

                Passcode passcode = new Passcode();
                int PassCodeNO = passcode.generateLongId();
                strPassCodeNO = String.valueOf(PassCodeNO);
                m_user.setMobileUserPassCode(strPassCodeNO);

                ORMConnection conn = new ORMConnection();

                Session session = conn.beginTransaction();

                Query namedQuery = session.getNamedQuery("MajlisMobileUsers.findByMobileUserMobileNo");
                namedQuery.setParameter("mobileUserMobileNo", mobileNo);

                List list = namedQuery.list();

                for (Object object : list) {
                    i++;
                }
                System.out.println("count:" + i);

                if (i == 0) {
                    Date Today = new Date();
                    m_user.setMobileUserDateRegistration(Today);

                    conn.createObject(session, m_user);

                    rd.setResponseFlag(0); // new user created

                    //send pass code via sms use the flag value
                } else if (i == 1) {

                    System.out.println("id passcode" + strPassCodeNO);

                    String selectQuery = "SELECT U FROM MajlisMobileUsers U WHERE mobileUserMobileNo ='" + mobileNo + "'";
                    List<Object> li = conn.hqlGetResults(selectQuery);

                    for (Object obj : li) {
                        MajlisMobileUsers user = new MajlisMobileUsers();
                        user = (MajlisMobileUsers) obj;

                        user.setMobileUserPassCode(strPassCodeNO);
                        session.clear();
                        conn.updateObject(session, user);
                    }

                    rd.setResponseFlag(1); // existing user pass code updated.

                } else {
                    rd.setResponseFlag(2); // more than one user available  
                }

                conn.commitObject(session);
                //conn.commitObject(session2);
                session.close();
                //session2.close();

            } else {
                rd.setResponseFlag(3); //null user name
            }
            rd.setPassCode(strPassCodeNO);
            return rd;

        } catch (Exception ex) {
            System.out.println("Error in MobileUserDetails method:  " + ex.getMessage());
            ex.printStackTrace();
            rd.setResponseFlag(4); //Exception occured
            rd.setPassCode("999");
        }

        return rd;
    }

    public ResponseData modifyMobileUserDetails(MajlisMobileUsers m_user) {
        System.out.println("modifyMobileUserDetails method call.................");

        int i = 0;
        String strPassCodeNO;
        ResponseData rd = new ResponseData(null,0,"",null);

        String mobileNo = m_user.getMobileUserMobileNo();
        String mobileUserName = m_user.getMobileUserName();

        Passcode passcode = new Passcode();
        int PassCodeNO = passcode.generateLongId();
        strPassCodeNO = String.valueOf(PassCodeNO);
        m_user.setMobileUserPassCode(strPassCodeNO);

        ORMConnection conn = new ORMConnection();

        Session session = conn.beginTransaction();

        try {

            if (mobileUserName != null) {

                Query namedQuery = session.getNamedQuery("MajlisMobileUsers.findByMobileUserMobileNo");
                namedQuery.setParameter("mobileUserMobileNo", mobileNo);

                List list = namedQuery.list();

                for (Object object : list) {
                    MajlisMobileUsers oldUserDetails = new MajlisMobileUsers();
                    oldUserDetails = (MajlisMobileUsers) object;
                    m_user.setMobileUserId(oldUserDetails.getMobileUserId());

                    Date modifiedDate = new Date();
                    m_user.setMobileUserDateModified(modifiedDate);

                    i++;

                }

                if (i == 0) {
                    rd.setResponseFlag(0); //not a registered user
                    //return rw;

                } else if (i == 1) {
                    session.clear();
                    conn.updateObject(session, m_user);
                    rd.setResponseFlag(1);  //sucess, registered user

                } else {
                    rd.setResponseFlag(2); // more than one user registered using the same mobile number
                }

                return rd;

            } else {
                rd.setResponseFlag(3);  //if mobile user name equals null
                return rd;
            }

        } catch (Exception e) {
            System.out.println("Error in MobileUserDetails method:  " + e.getMessage());
            rd.setResponseFlag(4); //Exception occured
            rd.setPassCode("999");

        }
        conn.commitObject(session);
        session.close();
        return rd;
    }

}
